//
//  WXForecastDataSource.h
//  Tutorial_SimpleWheater
//
//  Created by Filo on 20.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXForecastDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UIImageView *blurredImageView;

@end
