//
//  main.m
//  Tutorial_SimpleWheater
//
//  Created by Filo on 17.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
