//
//  WXCondition.h
//  Tutorial_SimpleWheater
//
//  Created by Filo on 19.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import "MTLModel.h"

@interface WXCondition : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *sunrise;
@property (strong, nonatomic) NSDate *sunset;
@property (strong, nonatomic) NSNumber *humidity;
@property (strong, nonatomic) NSNumber *temperature;
@property (strong, nonatomic) NSNumber *tempHigh;
@property (strong, nonatomic) NSNumber *tempLow;
@property (strong, nonatomic) NSNumber *windBearing;
@property (strong, nonatomic) NSNumber *windSpeed;
@property (copy, nonatomic) NSString *locationName;
@property (copy, nonatomic) NSString *conditionDescription;
@property (copy, nonatomic) NSString *condition;
@property (copy, nonatomic) NSString *icon;

- (NSString *)imageName;

@end
