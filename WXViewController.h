//
//  WXViewController.h
//  Tutorial_SimpleWheater
//
//  Created by Filo on 17.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WXViewController : UIViewController <UIScrollViewDelegate>

@end
