//
//  WXManager.h
//  Tutorial_SimpleWheater
//
//  Created by Filo on 19.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa/ReactiveCocoa.h>
#import "WXCondition.h"

// @import since Xcode 5:
@import CoreLocation;
@import Foundation;

@interface WXManager : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic, readonly) CLLocation *currentLocation;
@property (strong, nonatomic, readonly) WXCondition *currentCondition;
@property (strong, nonatomic, readonly) NSArray *hourlyForecast;
@property (strong, nonatomic, readonly) NSArray *dailyForecast;

+ (instancetype)sharedManager;

- (void)findCurrentLocation;

@end
