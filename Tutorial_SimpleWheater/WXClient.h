//
//  WXClient.h
//  Tutorial_SimpleWheater
//
//  Created by Filo on 19.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa/ReactiveCocoa.h>

// @import since Xcode 5:
@import CoreLocation;
@import Foundation;

@interface WXClient : NSObject

- (RACSignal *)fetchJSONFromURL: (NSURL *)url;
- (RACSignal *)fetchCurrentConditionsForLocation:(CLLocationCoordinate2D)coordinate;
- (RACSignal *)fetchHourlyForecastForLocation:(CLLocationCoordinate2D)coordinate;
- (RACSignal *)fetchDailyForecastForLocation:(CLLocationCoordinate2D)coordinate;

@end
