//
//  AppDelegate.h
//  Tutorial_SimpleWheater
//
//  Created by Filo on 17.01.2014.
//  Copyright (c) 2014 Filo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
